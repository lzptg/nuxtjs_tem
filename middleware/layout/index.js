import { get } from 'lodash';

/**
 * 页面布局显示: 根据需求显示不同的页面布局（全局中间件）
 *
 * @type {Array}
 */

export default function({ store, route, error, redirect, params, query, req, res }) {
    // console.log('route=>>', route)
    // store.commit('UPDATE_LAYOUT', [{ type: 'head', show: false }])
    // store.commit('UPDATE_LAYOUT', [{ type: 'search', show: false }])
    if(route.name === 'goods-id') {
        store.commit('UPDATE_LAYOUT', [{ type: 'right', show: false }])
    }
}