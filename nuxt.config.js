export default {
  ssr: true,
  target: 'server',
  telemetry: false,
  server: {
    port: 3010,
    host: '0.0.0.0',
  },
  head: {
    title: 'DEMO测试',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'demo测试' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: '//at.alicdn.com/t/font_1744638_3lxthu5sra2.css' }
    ]
  },
  css: [
    'element-ui/lib/theme-chalk/index.css',
    '@/assets/reset.min.css',
    '@/assets/common.css',
  ],
  plugins: [
    { src: '@/plugins/element-ui' },
    { src: '@/plugins/axios/axios' },
    { src: '@/plugins/api' },
    { src: '@/plugins/vue-global' },
    { src: '@/plugins/mock' }, // 模拟数据测试,不需要时关闭注释就可以了
  ],
  components: true, // 全局组件配置启用
  buildModules: [],
  loading: {
    color: '#1890ff',
  },
  router: {
    middleware: ['auth','layout/index','route/permit'], // 全局路由守卫配置
    mode: 'history', // 'history' 默认方式 | hash
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/style-resources',
    '@nuxtjs/pwa',
    '@nuxt/content',
    '@nuxtjs/proxy',
    'cookie-universal-nuxt',
  ],
  styleResources: {
    // scss: './assets/variables.scss',
    // less: './assets/variables.less',
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api/auth/login', method: 'post', propertyName: 'data.token' }, // json数据结构 | propertyName 参数就是返回的 response.data 内我们要获取的内容
          user: { url: '/api/auth/auth', method: 'post', propertyName: 'data' }, // json数据结构
          logout: { url: '/api/auth/logout', method: 'get' },
        }
      }
    },
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/'
    },
    cookie: {
      options: {
        maxAge: 60 * 60 * 24 * 7 // 一周
      }
    },
    localStorage: false
  },
  axios: {
    proxy: true,
    credentials: true,
  },
  proxy: [
    [
      '/api', {
        target: 'http://192.168.1.115:3000', // 代理地址
        changeOrigin: true,
      },
    ],
    [
      '/app', {
        target: 'https://www.ucharts.cn', // 代理地址
        changeOrigin: true,
        pathRewrite: {
            '^/app': ''
        }
      },
    ],
    [
      '/neigou', {
        target: 'https://api.xinminghui.com', // 代理地址
        changeOrigin: true,
      },
    ]
  ],
  content: {},
  build: {
    vendor: ['axios'],
    extractCSS: { allChunks: true },
    router: {
      extendRoutes (routes, resolve) {
        // 扩展路由，让 nuxt 支持静态页面请求，如 https://www.xxx.cn/index.html
        routes.push({
          name: 'index.html',
          path: '/index.html',
          component: resolve(__dirname, 'pages/index.vue'),
        });
      },
    },
  }
}
