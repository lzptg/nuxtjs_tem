export default ({ app: { $axios, $cookie } }, inject) => {
    const requestList = {}
    const methods = ['get', 'post', 'put', 'delete']
    methods.forEach(method => {
      let dataKey = method === 'get' ? 'params' : 'data'
      requestList[method] = function(url, data) {
        return $axios({
          method,
          url,
          [dataKey]: data
        }).catch(err => {
          console.error('error=>>', err)
          return {
            code: 0,
            data: {},
            message: err
          }
        })
      }
    })
    inject('api', requestList)
  }