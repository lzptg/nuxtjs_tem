/**
 * 拦截器：给 request Header 统一增加 Header
 *
 * header : {
 *     Authorization: token
 * }
 *
 * @type {Array}
 */
import { get } from 'lodash';

const replaceStr = str => str && str.replace(/(\s+|Bearer)/ig, '')

const authorizationInterceptor = [
    config => {
        config.headers['uid'] = '7796';
        const token = get(config, 'headers.common.Authorization');
        if (token) {
            config.headers['Accept-Token'] = replaceStr(token);
        }
        return config;
    },
    error => {
        throw error;
    }
];

export default authorizationInterceptor;
