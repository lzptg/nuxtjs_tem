import Vue from 'vue'
// import loginModal from '~/components/common/loginModal'
// import '~/assets/scss/element-variables.scss' // elementUI 自定义主题色
import utils from '~/utils/index'

// Vue.use(loginModal)
// Vue.filter('formatTime', d => utils.formatTime(d))

export default function (context, inject) {
  inject('utils', utils) // 全局注册
}