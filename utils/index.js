export default {
    toObject(json) {
      let res = {}
      try {
        res = JSON.parse(json)
      } catch (error) {
        console.error(error)
      }
      return res
    },
  }