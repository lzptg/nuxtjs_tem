/**
*   const io = IntersectionObserver(callback, options)
*   callback 是当被监听元素的可见性变化时，触发的回调函数
*   options 是一个配置参数，可选，有默认的属性值
*
*   io.observe(DOM)         开始观察，接受一个DOM节点对象
*   io.unobserve(element)   停止观察 接受一个element元素
*   io.disconnect()         关闭观察器
*/

function lazyLoad (imgs) {
    const options = {
        // root: null, // 指定观察边界
        // threshold: [0, 0.5, 1], // 边界交叉比例在%0、%50、%100时进行回调
        rootMargin: '30px', //  扩大边界交叉范围
    };
    let count = 0;
    const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (entry.intersectionRatio > 0) {
                entry.target.src = entry.target.getAttribute('data-src');
                count++;
                observer.unobserve(entry.target);
                if (count === imgs.length) {
                    window.removeEventListener('load', handler);
                }
            }
        });
    }, options);
    for (let i = 0,length = imgs.length; i < length; i++) {
        observer.observe(imgs[i]);
    }
}
const imgs = document.querySelectorAll('img');
const handler = function () {
    lazyLoad(imgs);
};
window.addEventListener('load', handler);


// 较早兼容性最好版本
// 获取窗口高度
// function getWindowHeight () {
//     return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
// }
// function getTop (e) {
//     let t = e.offsetTop;
//     while (e = e.offsetParent) {
//         t += e.offsetTop;
//     }
//     return t;
// }
// const delta = 30;
// let count = 0;
// function lazyLoad (imgs) {
//     const winH = getWindowHeight();
//     const s = document.documentElement.scrollTop || document.body.scrollTop;
//     for (let i = 0, l = imgs.length; i < l; i++) {
//         if (winH + s + delta > getTop(imgs[i]) && getTop(imgs[i]) + imgs[i].offsetHeight + delta > s) {
//             if (!imgs[i].src) {
//                 imgs[i].src = imgs[i].getAttribute('data-src');
//             	count++;
//             }
//             if (count === l) {
//                 window.removeEventListener('scroll', handler);
//                 window.removeEventListener('load', handler);
//             }
//         }
//     }	
// }
// const imgs = document.querySelectorAll('img');
// const handler = function () {
//     lazyLoad(imgs);
// };
// window.addEventListener('scroll', handler);
// window.addEventListener('load', handler);